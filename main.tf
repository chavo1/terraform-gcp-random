resource "random_pet" "server2" {
  keepers = {}
}

output "pet_name" {
  value = "${random_pet.server2.id}"
}

resource "null_resource" "env" {
  provisioner "local-exec" {
    command = "env"
  }
}

resource "null_resource" "env1" {
  provisioner "local-exec" {
    command = "echo $ATLAS_CONFIGURATION_VERSION_GITHUB_COMMIT_SHA"
  }
}

resource "null_resource" "env2" {
  provisioner "local-exec" {
    command = "ls -la"
  }
}
